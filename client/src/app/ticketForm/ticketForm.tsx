import { Button, FormControl, FormLabel, Input, Stack } from "@chakra-ui/react";
import { useState } from "react";
import { useMutation, useQueryClient } from "react-query";
import { createTicket } from "../data/REST/ticket";

export function TicketForm() {
  const client = useQueryClient();
  const [description, setDescription] = useState('');
  
  const onSuccess = () => {
      client.invalidateQueries('tickets');
  }

  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setDescription(event.target.value);
  };

  const create = useMutation(createTicket, { onSuccess });

  return (<Stack>
    <FormControl isRequired>
      <FormLabel>Ticket description:</FormLabel>
      <Input placeholder='Ticket description' onChange={handleChange} value={description} />
      <Button colorScheme='teal' size='xs' onClick={() => create.mutate(description) } >Add ticket</Button>
    </FormControl>
  </Stack>)
}