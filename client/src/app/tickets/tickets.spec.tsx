import { render, waitFor, screen } from '@testing-library/react';
import { QueryClient, QueryClientProvider } from 'react-query';
import nock from 'nock';

import Tickets from './tickets';

const scope = nock('http://localhost/api/').defaultReplyHeaders({
  'access-control-allow-origin': '*',
});
let queryClient: QueryClient;

describe('Tickets', () => {
  beforeEach(() => {
    queryClient = new QueryClient({
      defaultOptions: {
        queries: {
          retry: false,
          refetchOnWindowFocus: false,
        },
      },
    });
  });

  it('should render successfully with "Loading" state', async () => {
    scope
      .get('/tickets/')
      .once()
      .reply(200, [
        {
          id: 1,
          description: 'Install a monitor arm',
          assigneeId: 1,
          completed: false,
        },
        {
          id: 2,
          description: 'Move the desk to the new location',
          assigneeId: 1,
          completed: false,
        },
      ]);

    render(
      <QueryClientProvider client={queryClient}>
        <Tickets />
      </QueryClientProvider>
    );

    expect(screen.getByText('Loading')).toBeInTheDocument();
  });
  it('should render successfully with "Error" state', async () => {
    scope.get('/tickets/').once().reply(500, 'Server Unavailable');

    render(
      <QueryClientProvider client={queryClient}>
        <Tickets />
      </QueryClientProvider>
    );

    await waitFor(() => {
      expect(screen.getByText('Problem fetching data')).toBeInTheDocument();
    });
  });

  it('should render successfully with "Error" state', async () => {
    scope
      .get('/tickets/')
      .once()
      .reply(200, [
        {
          id: 1,
          description: 'Install a monitor arm',
          assigneeId: 1,
          completed: false,
        },
        {
          id: 2,
          description: 'Move the desk to the new location',
          assigneeId: 1,
          completed: false,
        },
      ]);

    render(
      <QueryClientProvider client={queryClient}>
        <Tickets />
      </QueryClientProvider>
    );

    await waitFor(() => {
      expect(screen.getByText('Install a monitor arm')).toBeInTheDocument();
      expect(
        screen.getByText('Move the desk to the new location')
      ).toBeInTheDocument();
    });
  });
});
