import { useState } from 'react';
import { Badge, Link, Box, Menu, MenuButton, Button, MenuList, MenuOptionGroup, MenuItemOption, Stack } from '@chakra-ui/react'
import { Ticket } from '@acme/shared-models';
import { useQuery } from 'react-query';
import { fetchTickets } from '../data/REST/ticket';
import { TicketForm } from '../ticketForm/ticketForm';

export function TicketList() {
  const [selectedFilter, setSelectedFilter] = useState('all');

  const { data, isLoading, isError, error } = useQuery<Ticket[], Error>(["tickets"], fetchTickets);

  const filterConditions = (ticket: Ticket) => {
    if (selectedFilter === 'all') {
      return true;
    }
    return selectedFilter === 'incomplete' ? !ticket.completed : ticket.completed;
  }

  if (isLoading) { 
    return (<Badge
      colorScheme="purple"
      variant="outline"
      borderRadius="4"
      p='4' m='5'
    >Loading</Badge>);
  }

  if (isError) { 
    return (<Badge
      colorScheme="purple"
      variant="outline"
      borderRadius="4"
      p='4' m='5'
    >{ error.message }</Badge>);
  }

  if (!data) {
    return (<Badge
      colorScheme="purple"
      variant="outline"
      borderRadius="4"
      p='4' m='5'
    >The record does not exist</Badge>);
  }

  return (
        <Stack spacing="24px" w="320px">
          <Box>
            <Menu>
              <MenuButton as={Button}>
                Filter: {selectedFilter}
              </MenuButton>
              <MenuList>
                <MenuOptionGroup defaultValue={selectedFilter} type='radio' onChange={(ev) => setSelectedFilter(ev as string)}>
                  <MenuItemOption value='all'>All</MenuItemOption>
                  <MenuItemOption value='incomplete'>Incomplete</MenuItemOption>
                  <MenuItemOption value='complete'>Complete</MenuItemOption>
                </MenuOptionGroup>
              </MenuList>
            </Menu>  
          </Box>
            <ul>
            {data.filter(filterConditions).map((ticket) => (
              <li key={ticket.id}>
                <Link href={`/${ticket.id}`}>{ticket.description}</Link>
              </li>
            ))}
            </ul>
          <Box>
        <TicketForm />
      </Box>
    </Stack>);
}

export default TicketList;
