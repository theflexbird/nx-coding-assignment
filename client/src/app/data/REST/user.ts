import { User } from "@acme/shared-models";
import { verifyResponse } from "../../helper/error";

export const fetchUser = async (userId: number | null | undefined): Promise<User> => {
  if (userId === null || userId === undefined) { 
    return {} as User;
  }
  const response = await fetch(`/api/users/${userId}`);
  verifyResponse(response);
  return response.json();
}

export const fetchUsers = async (): Promise<User[]> => {
  const response = await fetch(`/api/users/`);
  verifyResponse(response);
  return response.json();
}

export const assignUserRequest = async (params: { userId: number, ticketId: number } ): Promise<boolean> => {
  const response = await fetch(`/api/tickets/${params.ticketId}/assign/${params.userId}`, {
    method: 'PUT'
  });
  verifyResponse(response);
  return response.ok;
}

export const unassignUserRequest = async (ticketId: number): Promise<boolean> => {
  const response = await fetch(`/api/tickets/${ticketId}/unassign`, {
    method: 'PUT'
  });
  verifyResponse(response);
  return response.ok;
}