import { Ticket } from "@acme/shared-models";
import { verifyResponse } from "../../helper/error";

export const fetchTicket = async (ticketId: string): Promise<Ticket> => {
  const response = await fetch(`/api/tickets/${ticketId}`);
  verifyResponse(response);
  return response.json();
}

export const fetchTickets = async (): Promise<Ticket[]> => {
  const response = await fetch(`/api/tickets/`);
  verifyResponse(response);
  return response.json();
}

export const completeStatus = async (ticketId: number): Promise<boolean> => {
  const response = await fetch(`/api/tickets/${ticketId}/complete`, {
    method: 'PUT'
  });
  verifyResponse(response);
  return response.ok;
}

export const incompleteStatus = async (ticketId: number): Promise<boolean> => {
  const response = await fetch(`/api/tickets/${ticketId}/complete`, {
    method: 'DELETE'
  });
  verifyResponse(response);
  return response.ok;
}

export const createTicket = async (ticketDescription: string): Promise<Ticket> => {
  const response = await fetch(`/api/tickets`, {
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    },
    method: 'POST',
    body: JSON.stringify({ description: ticketDescription })
  });
  verifyResponse(response);
  return response.json();
}