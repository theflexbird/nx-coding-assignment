import { Routes, Route } from 'react-router-dom';
import { VStack,Text } from "@chakra-ui/react"
import TicketList from './tickets/tickets';
import { TicketComponent } from './ticket/ticket';

const App = () => {
  return (
    <VStack p={5}>
      <Text bgGradient="linear(to-l, #7928CA,#FF0080)"
      bgClip="text"
      fontSize="6xl"
      fontWeight="extrabold">
        Ticketing App
      </Text>
      <Routes>
        <Route path="/" element={<TicketList />} />
        {/* Hint: Try `npx nx g component TicketDetails --project=client --no-export` to generate this component  */}
        <Route path="/:id" element={<TicketComponent />} />
      </Routes>
    </VStack>
  );
};

export default App;
