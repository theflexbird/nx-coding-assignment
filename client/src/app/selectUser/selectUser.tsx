import { User } from "@acme/shared-models";
import { Button, Menu, MenuButton, MenuItemOption, MenuList, MenuOptionGroup, Text } from "@chakra-ui/react";
import { useMutation, useQuery, useQueryClient } from "react-query";
import { assignUserRequest, fetchUsers, unassignUserRequest } from "../data/REST/user";

interface Props {
  user: User,
  ticketId: number,
}

export function SelectUser(props: Props) {
  const client = useQueryClient();
  const { data: users, isLoading, isError } = useQuery<User[], Error>(["users"], fetchUsers);
  const onSuccess = () => {
      client.invalidateQueries('ticket');
  }

  const assignUserMutation = useMutation(assignUserRequest, { onSuccess });

  const unassignUserMutation = useMutation(unassignUserRequest, { onSuccess });


  const assignUser = (userId: string | undefined) => {
    const id = Number(userId);
    if (id) {
      assignUserMutation.mutate({ userId: id, ticketId: props.ticketId })
    }
    else {
      unassignUserMutation.mutate(props.ticketId);
    }
  }

  if (isLoading) { 
    return (
      <Text>Loading users</Text>
    );
  }

  if (isError) { 
    return (
      <Text>Error while loading users</Text>
    );
  }

  if (!users) {
    return (
      <Text>User list is empty</Text>
    )
  }

  return (<Menu>
              <MenuButton as={Button}>
                Assignee: {props.user.name || "unassign" }
              </MenuButton>
                <MenuList>
                  <MenuOptionGroup defaultValue={`${props.user.id}`} type='radio' onChange={(ev) => assignUser(ev as string)}>
                    <MenuItemOption value="undefined" key="-1">unassign</MenuItemOption>
                    {users.map(user => (<MenuItemOption value={`${user.id}`} key={user.id}>{user.name}</MenuItemOption>))}
                  </MenuOptionGroup>
                </MenuList>
            </Menu>);
}