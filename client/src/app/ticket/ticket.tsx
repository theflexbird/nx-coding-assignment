import { Text, Badge, Stack, Button } from '@chakra-ui/react'
import { useParams } from 'react-router-dom';
import { useMutation, useQuery, useQueryClient } from 'react-query';
import { completeStatus, fetchTicket, incompleteStatus } from '../data/REST/ticket';
import { Ticket, User } from '@acme/shared-models';
import { fetchUser } from '../data/REST/user';
import { SelectUser } from '../selectUser/selectUser';

export function TicketComponent() {
  const { id } = useParams();
  const client = useQueryClient();
  
  const { data: ticket, isLoading: isLoadingTicket, isError: isErrorTicket } = useQuery<Ticket, Error>(["ticket", id], () => fetchTicket(id!));
  const { data: user, isLoading: isLoadingUser, isError: isErrorUser } = useQuery<User, Error>(["user", ticket], () => fetchUser(ticket?.assigneeId));

  const onSuccess = () => {
    client.invalidateQueries('ticket');
  }

  const changeStatusToComplete = useMutation(completeStatus, { onSuccess });

  const changeStatusToIncomplete = useMutation(incompleteStatus, { onSuccess });

  const toggleStatus = async (ticket: Ticket) => {
    // TODO: add some loading status
    if (ticket.completed) {
      changeStatusToIncomplete.mutate(ticket.id);
    }
    else {
      changeStatusToComplete.mutate(ticket.id);
    }
  }

  if (isLoadingTicket || isLoadingUser) { 
    return (<Badge
      colorScheme="purple"
      variant="outline"
      borderRadius="4"
      p='4' m='5'
    >Loading</Badge>);
  }

  if (isErrorTicket || isErrorUser) { 
    return (<Badge
      colorScheme="purple"
      variant="outline"
      borderRadius="4"
      p='4' m='5'
    >Unexpected fetching error</Badge>);
  }

  if (!ticket || !user) {
    return (<Badge
      colorScheme="purple"
      variant="outline"
      borderRadius="4"
      p='4' m='5'
    >The record does not exist</Badge>);
  }

    return (
          <Stack spacing="24px" w="320px">
            <Text>Description: {ticket.description}</Text>
            <Button onClick={() => toggleStatus(ticket)}> Status: {ticket.completed ? 'Complete' : 'Incomplete'}</Button>
            <SelectUser user={user} ticketId={ticket.id}></SelectUser>
          </Stack>);
}

export default TicketComponent;
