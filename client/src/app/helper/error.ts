export const verifyResponse = (response: Response) => {
  if (!response.ok) {
    throw new Error("Problem fetching data");
  }
}